﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TitleDetails.aspx.cs" Inherits="TitleDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Title Details</title>
    <style type="text/css">
        .auto-style1 {
            width: 128px;
        }

        .auto-style2 {
            width: 149px;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function divexpandcollapse(divname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            if (div.style.display == "none") {
                div.style.display = "inline";
                img.src = "Images/minus.jpg";
            } else {
                div.style.display = "none";
                img.src = "Images/plus.jpg";
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="height: 90px; width: 425px">
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="lblTitle" Text="Enter Title" runat="server"></asp:Label>
                    </td>
                    <td class="auto-style2">
                        <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td rowspan="2"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="auto-style2">
                        <asp:Button ID="btnSearch" Text="Search" runat="server" OnClick="btnSearch_Click" />
                        &nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnClear" Text="Clear" runat="server" OnClick="btnClear_Click" />
                    </td>
                </tr>
                <tr>
                    <td rowspan="2"></td>
                </tr>
                <tr>
                    <td rowspan="2" class="auto-style2">
                        <asp:GridView ID="gvTitle" runat="server" AllowPaging="true" GridLines="None"  AllowSorting="true" AutoGenerateColumns="false" OnRowDataBound="gvTitle_RowDataBound" BorderColor="#df5015">
                            <HeaderStyle BackColor="#df5015" Font-Bold="true" ForeColor="White" />
                            <RowStyle BackColor="#E1E1E1" />
                            <AlternatingRowStyle BackColor="White" />
                            <HeaderStyle BackColor="#df5015" Font-Bold="true" ForeColor="White" />
                            <Columns>
                                <asp:TemplateField ItemStyle-Width="20px">
                                    <ItemTemplate>
                                        <a href="JavaScript:divexpandcollapse('div<%# Eval("TitleId") %>');">
                                            <img id="imgdiv<%# Eval("TitleId") %>" width="9px" border="0" src="Images/plus.jpg" />
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Title" HeaderText="Titles" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <tr>
                                            <td colspan="100%">
                                                <div id="div<%# Eval("TitleId") %>" style="display: none; position: relative; left: 15px; overflow: auto;">
                                                    <asp:GridView ID="gvChildGrid" runat="server" AutoGenerateColumns="false" BorderStyle="Double"
                                                         BorderColor="#df5015" GridLines="None" Width="850px" HorizontalAlign="Center" >
                                                        <HeaderStyle BackColor="#df5015" Font-Bold="true" ForeColor="White" />
                                                        <RowStyle BackColor="#E1E1E1" />
                                                        <AlternatingRowStyle BackColor="White" />
                                                        <HeaderStyle BackColor="#df5015" Font-Bold="true" ForeColor="White" />
                                                        <Columns>
                                                            <asp:BoundField DataField="Type" HeaderText="Type" HeaderStyle-HorizontalAlign="Left" />
                                                            <asp:BoundField DataField="Description" HeaderText="Description" HeaderStyle-HorizontalAlign="Left" />
                                                             <asp:BoundField DataField="Language" HeaderText="Language" HeaderStyle-HorizontalAlign="Left" />
                                                        </Columns>
                                                    </asp:GridView>
                                                    <br />
                                                    <br />
                                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" BorderStyle="Double"
                                                         BorderColor="#df5015" GridLines="None" Width="850px" HorizontalAlign="Center" >
                                                        <HeaderStyle BackColor="#df5015" Font-Bold="true" ForeColor="White" />
                                                        <RowStyle BackColor="#E1E1E1" />
                                                        <AlternatingRowStyle BackColor="White" />
                                                        <HeaderStyle BackColor="#df5015" Font-Bold="true" ForeColor="White" />
                                                        <Columns>
                                                            <asp:BoundField DataField="AwardCompany" HeaderText="Award Company" HeaderStyle-HorizontalAlign="Left" />
                                                            <asp:BoundField DataField="AwardWon" HeaderText="Award Won" HeaderStyle-HorizontalAlign="Left" />
                                                             <asp:BoundField DataField="AwardYear" HeaderText="Award Year" HeaderStyle-HorizontalAlign="Left" />
                                                            <asp:BoundField DataField="Award1" HeaderText="Award" HeaderStyle-HorizontalAlign="Left" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
