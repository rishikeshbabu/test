﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class TitleDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        gvTitle.Visible = false;
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        gvTitle.Visible = true;
        TitlesEntities dbContext = new TitlesEntities();
        var getTitle = (from t in dbContext.Titles
                        where t.TitleName.Contains(txtTitle.Text)
                        select new { Title = t.TitleName, TitleId = t.TitleId  })
                        .ToList();
        gvTitle.DataSource = getTitle;
        gvTitle.DataBind();
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        gvTitle.Visible = false;
        txtTitle.Text = string.Empty;
    }

    protected void gvTitle_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            GridView gv = (GridView)e.Row.FindControl("gvChildGrid");
            GridView gv1 = (GridView)e.Row.FindControl("GridView1");
            string TitleName = e.Row.Cells[1].Text;
            TitlesEntities dbContext = new TitlesEntities();
            var getDescription = (from t in dbContext.Titles
                            from s in dbContext.StoryLines 
                            where t.TitleName == TitleName && t.TitleId == s.TitleId
                            select new { Type = s.Type, Description = s.Description, Language = s.Language })
                            .ToList ();
            gv.DataSource = getDescription;
            gv.DataBind();

            var getAward = (from t in dbContext.Titles
                            from a in dbContext.Awards 
                            where t.TitleName == TitleName && t.TitleId == a.TitleId
                            select new { AwardCompany = a.AwardCompany, AwardWon = a.AwardWon, AwardYear = a.AwardYear, Award1  = a.Award1 })
                           .ToList();

            gv1.DataSource = getAward;
            gv1.DataBind();
        }
    }
}